str1 = 'this is single quote string'
str2 = "this is double quote string"
str3 = """ this is 
triple quote 
string """
str4 = r"this is \n raw string"
print(str1)
print(str2)
print(str3)

def test_function():
	""" this function is to check for doc which stores info about function """
	value=10
	return value
	
print(str4)
print(test_function.__doc__)

