num_list = [1, 7, 12, 28, 33, 50]
mix_list = [15,"second","third",62,[1,2,3,4]]

ar = [1,2,3,4,5]

a = ['spam', 'egg', 'bacon', 'tomato', 'ham', 'lobster']

num_list.append(67)
print(num_list)
num_list.insert(3,21)
print(num_list)
num_list.pop(3)
print(num_list)
num_list.remove(33)
print(num_list)
num_list.extend([43,67,99,110])
print(num_list)

print("slicing {}".format(num_list[1:len(num_list)-1]))
print("values at even position {}".format(num_list[::2]))
print("reverse the list {}".format(num_list[::-1]))

# using in operator
print("second" in mix_list)

#list comprehension

new_list = [2 * i for i in ar]
print(new_list)
