
def test1(player,jersey_no,country):
	print(f'{player} with {jersey_no} plays for {country}')


#exapmle of positional argument
test1('messi',10,'arg')

#exapmle of keyword argument
test1(player='messi', jersey_no=10, country='arg') 
#ordering of arguments can be changed
test1(player='rooney', country='eng', jersey_no=15)
#mixin positional and keyword for which first specify all positional and then keyword and once we start providing keyword after postional , all arguments should be passed as keyword .

test1('rooney', country='eng', jersey_no=15)

#tihs will throw an error because we started kerwork based from jersey_no and country should also be keyword argument

test1('rooney','eng', jersey_no=15)

#default parameters 

def test2(player='chetri',jersey_no=9,country='india'):
	print(f'{player} with {jersey_no} plays for {country}')

test2()

test2('mandeep',jersey_no=15)

#argument packing : used when we provide more arguments than specified in parameter
args will be converted into tuple by default
def test3(*args):
    print(args)
    print(type(args), len(args))
    for i in args:
        print(i)

test3()

test3(1,2.34,"swesd",[1,4,3,7,12],(23,31,43,34))

#argument unpacking

def f(x, y, z):
    print(f'x = {x}')
    print(f'y = {y}')
    print(f'z = {z}')

t = (10,20,[30,40,50])
f(*t)

# argument packing for dictionary and keyword arguments 
def fun(**args):
	print(type(args),len(args))
	 for i in args:
		print(i,args.get(i))

fun(a=2,b=3)


# Assignemnt: to solve the problem occuring in this code
def f(my_list=[]):
    my_list.append('###')
    return my_list

#we will provide None in default paramenter and check if list is none for every time
def f(my_list=None):
	if my_list==None:
		my_list = []
	my_list.append('###')
	return my_list
	
	
