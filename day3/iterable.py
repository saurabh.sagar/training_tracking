my_list = [6,34,51,78,29,62]
conv_to_tuple = tuple(my_list)

str = 'learning iterables fucntions'
#iterating to list
conv_to_list = list(str)

#sorted is an iterable function which sorts list inplace

print(sorted(my_list))

#max , min and sum are also an iterables
print(sum(my_list))
print(min(my_list))
print(max(my_list))

#all will return true if all the boolean values are true otherwise false
#any will return false if all the boolean values are false otherwise true

list1 = [true, ture, false, false, true, true]
print("any for list1",any(list1),", all for list1",all(list1))

list2 = [1 , 0 , 0 , 1 , 1 , 1, 0]
print("any for list2",any(list2),", all for list2 slicing",all(list2[3:6])

#it contains ony one boolean values
list3 = [ 3, 5, 7,13,17,1,22])
print("any for list3",any(list3),", all for list2 slicing",all(list3))

print("these item will be evaluated as fasle for boolean : [0, None,False, [], ""])
print("these item will be evaluated as true for boolean : [1, 'hello', True,[2,3,4], (8,9)]")

#using zip
Fruits = ['Apple', 'Banana', 'Mango']
Colours = ['Red', 'Yellow', 'Green']
Numbers = [1,2,3]

print(list(zip(Numbers,Fruits,Colours)))


#enumeration - gives the index and each element as tuple in any iterable

for i in enumerate("E2ENETWORKS"):
	print(i)
#use of unpacking in enumerate
none_ind = []
for item_ind,item in enumerate([ 1 ,None, 4 , 7, None]):
	if item is None:
		none_ind.append(item_ind)
print(none_ind)

#__iter__ and __next__ are underlying fucntions used in for loop 
my_list = [6,34,51,78,29,62]
iter_obj = iter(my_list)

print(next(iter_obj))  #output is 6

print(next(iter_obj))  #output is 34

print(next(iter_obj))  #output is 51

print(next(iter_obj))  #output is 78

print(next(iter_obj))  #output is 29

print(next(iter_obj))  #output is 62

print(next(iter_obj))  #error of StopIteration

#how for loop is implemented underlying using iterators

iter_obj = iter(iterable)   #iterable is any object that is iterable

while True:
	try:
		element = next(iter_obj)
	except StopIteration:
		break


Assignment : 
A generator is used to produce custom iterable objects. It does not contain return statement instead it uses yield.
 example:
 
 def test_generator():
 	print('First item')
    	yield 10

    	print('Second item')
    	yield 20

    	print('Last item')
    	yield 30 

gen = test_generator()   # gen is and iterable object on which we can use next

print(next(gen))

print(next(gen))

print(next(gen))

The difference between yield and return is that yield returns a value and pauses the execution while maintaining the internal states, whereas the return statement returns a value and terminates the execution of the function




